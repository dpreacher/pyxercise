#!/usr/bin/env python3

import time


start_time = time.time()

duration = 2 * 60

count = 0

while time.time() - start_time <= duration:
    print(f"{count=}")
    count += 1
    time.sleep(33)
