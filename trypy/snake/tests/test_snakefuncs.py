from ..snakeladder_lib import SnakeLib

def test_roll_die_exceed_100():
    """Check if the player position exceeds 100"""
    new_pos = SnakeLib()._roll_die(99)
    assert new_pos <= 100, "no position after 100"

def test_jump_tracker_snake():
    """Check if the player position correctly goes down at a snake mouth"""
    new_pos = SnakeLib()._jump_tracker(84)
    assert (new_pos == 58), "expected player to go to 58"

def test_jump_tracker_ladder():
    """Check if the player position correctly goes up at a ladder base"""
    new_pos = SnakeLib()._jump_tracker(57)
    assert (new_pos == 96), "expected climb to 96"

def test_jump_tracker_stay():
    """check that player position does not change at location with no ladder and no snake"""
    new_pos = SnakeLib()._jump_tracker(83)
    assert (new_pos == 83), "Y U JUMP AA?"