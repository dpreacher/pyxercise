#!/usr/bin/env python3

from snakeladder_lib import SnakeLib

""" Snakes and Ladders."""

if __name__ == "__main__":
    sl_game = SnakeLib()
    print("Do you want to play the game turn by turn or do you want the computer to simulate all moves")
    patience_level = input("Manual (M) or Automatic (A)? ")
    if patience_level.upper() == 'M':
        game_results = sl_game.game_play()
    elif patience_level.upper() == 'A':
        game_results = sl_game.who_won()
    who_won = game_results[0]
    final_score = game_results[1]
    print (f"{who_won} has won the game")
    for player in final_score.keys():
        if player != who_won:
            print(f"{player} reached only till {final_score[player]}")