from random import randint


class SnakeLib:
    """Class for snakes and ladders game functions and data. """

    def __init__(self):
        self.pdata = {}
        self.player_count = 0
        self._jumps = self._add_snakes_ladders()

    def init_scores(self):
        print(f"Playing with {self._jumps}")
        self.player_count = int(input("How many of you are playing: "))
        for i in range(self.player_count):
            p_name = input(f"{i + 1})Enter name of player: ")
            self.pdata[p_name] = 0

    def _add_snakes_ladders(self):
        jumpdict = {}
        _jump_count = randint(2, 26)
        for _ in range(_jump_count):
            from_val = randint(1, 99)
            to_val = randint(1, 99)
            while (
                (abs(from_val - to_val) <= 19) or (abs(from_val - to_val) >= 30)
                or (from_val in jumpdict.keys()) or (from_val in jumpdict.values())
                or (to_val in jumpdict.keys()) or (to_val in jumpdict.values())
            ):
                from_val = randint(1, 99)
                to_val = randint(1, 99)
            jumpdict[from_val] = to_val
        return jumpdict

    def _roll_die(self, curr_pos):
        dieside = 0
        sixcounter = 0
        while dieside == 0 or sixcounter in range(1, 3):
            dieside = randint(1, 6)
            if curr_pos + dieside > 100:
                return curr_pos
            if dieside == 6:
                sixcounter += 1
            else:
                dieside += sixcounter * 6
                sixcounter = 0
        if sixcounter == 0 and (curr_pos + dieside <= 100):
            return curr_pos + dieside
        else:
            return curr_pos

    def _jump_tracker(self, curr_pos):
        if curr_pos in self._jumps.keys():
            return self._jumps[curr_pos]
        else:
            return curr_pos

    def game_play(self):
        self.init_scores()
        while 100 not in self.pdata.values():
            for player in self.pdata.keys():
                curr_value = self.pdata[player]
                print(f"\n{player}, press Enter to roll the dice")
                input()
                self.pdata[player] = self._roll_die(self.pdata[player])
                if self.pdata[player] == curr_value:
                    print(f"{player} is still at {self.pdata[player]}")
                else:
                    print(f"{player} moved to {self.pdata[player]}")
                zapped_to = self._jump_tracker(self.pdata[player])
                if zapped_to < self.pdata[player]:
                    self.pdata[player] = zapped_to
                    print(f"YUM! YUM! {player} went down a snake belly to {self.pdata[player]}")
                while zapped_to > self.pdata[player]:
                    self.pdata[player] = zapped_to
                    print(f"WHOA! {player} climbed the ladder to {self.pdata[player]}")
                    print(f"{player} will get another chance")
                    curr_value = self.pdata[player]
                    print(f"\n{player}, press Enter to roll the dice")
                    input()
                    self.pdata[player] = self._roll_die(self.pdata[player])
                    if self.pdata[player] == curr_value:
                        print(f"{player} is still at {self.pdata[player]}")
                    else:
                        print(f"{player} moved to {self.pdata[player]}")
                    zapped_to = self._jump_tracker(self.pdata[player])
                    if zapped_to < self.pdata[player]:
                        self.pdata[player] = zapped_to
                        print(f"YUM! YUM! {player} went down a snake belly to {self.pdata[player]}")
                if self.pdata[player] == 100:
                    return (player, self.pdata)

    def who_won(self):
        self.init_scores()
        while 100 not in self.pdata.values():
            for player in self.pdata.keys():
                curr_value = self.pdata[player]
                self.pdata[player] = self._roll_die(self.pdata[player])
                zapped_to = self._jump_tracker(self.pdata[player])
                if zapped_to < self.pdata[player]:
                    self.pdata[player] = zapped_to
                while zapped_to > self.pdata[player]:
                    self.pdata[player] = zapped_to
                    curr_value = self.pdata[player]
                    self.pdata[player] = self._roll_die(self.pdata[player])
                    zapped_to = self._jump_tracker(self.pdata[player])
                    if zapped_to < self.pdata[player]:
                        self.pdata[player] = zapped_to
                if self.pdata[player] == 100:
                    return (player, self.pdata)
