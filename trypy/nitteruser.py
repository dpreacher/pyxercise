#!/usr/bin/env python3
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

driver = webdriver.Firefox()
driver.get("http://nitter.snopyta.org")
username = input("Enter the username you want to search: ")
driver.find_element_by_xpath("/html/body/div/div/div/form/input[2]").send_keys(username)
driver.find_element_by_xpath("/html/body/div/div/div/form/button").submit()
elems = WebDriverWait(driver, 10).until(EC.presence_of_all_elements_located((By.CLASS_NAME, "username")))
results = driver.find_elements_by_class_name("username")
for i in range(len(results)):
    print(f"{i}: {results[i].text}")
userlink = int(input("Enter the number next to the username you want to click: "))
if userlink in range(0, len(results)):
    driver.find_elements_by_class_name("username")[userlink].click()
    elems = WebDriverWait(driver, 10).until(
        EC.presence_of_all_elements_located((By.CLASS_NAME, "icon-rss-feed"))
    )
    results = driver.find_elements_by_class_name("icon-rss-feed")
    print(f"{results[0].get_attribute('href')}")
    input("Press a key to exit\n")
    driver.quit()
