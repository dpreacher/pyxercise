#!/usr/bin/env python3
import re


def pwqc(pwd):
    """checks password strength using predetermined conditions"""
    if (len(pwd) > 5) and (len(pwd) < 13):
        if (
            (re.search("[A-Z]", pwd))
            and (re.search("[a-z]", pwd))
            and (re.search("[0-9]", pwd))
            and (re.search("[$#@]", pwd))
        ):
            return True
    return False


def main(args):
    print (pwqc.__doc__)
    pw_list = ",".join(
        pw for pw in input("Enter comma separated passwords: ").split(",") if pwqc(pw)
    )
    print(pw_list)
    return 0


if __name__ == "__main__":
    import sys

    sys.exit(main(sys.argv))
