#!/usr/bin/env python3

def dict_squares(num):
    ans_dict = {}
    for _ in range(1, num + 1):
        ans_dict[_] = _ ** 2
    return ans_dict

def main(args):
    num = int(input('Enter an integer to build a dictionary of its squares: '))
    print (dict_squares(num))
    return 0

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
