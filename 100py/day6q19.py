#!/usr/bin/env python3

def main(args):
    mark_list = []
    print ('Enter one record per line with format name,age,score. enter blank line to stop')
    while True:
        rec = input().split(',')
        if not rec[0] or len(rec) < 3:
            break
        mark_list.append(tuple(rec))
    mark_list.sort(key= lambda x:(x[0], int(x[1]), int(x[2])))
    print (mark_list)
    return 0

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))