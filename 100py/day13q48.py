#!/usr/bin/env python3


class Rectangle:
    """class to compute area of rectangle"""

    def __init__(self, length, breadth):
        self.length = length
        self.breadth = breadth

    def rectangle_area(self):
        return self.length * self.breadth


def main(args):
    length, breadth = tuple(
        input("Enter the length and breadth of the rectangle separated by comma: ").split(
            ","
        )
    )
    print(f"{Rectangle(float(length), float(breadth)).rectangle_area():.2f}")
    return 0


if __name__ == "__main__":
    import sys

    sys.exit(main(sys.argv))
