#!/usr/bin/env python3


def main(args):
    while True:
        num_list = input('Enter numbers separated by commas: ')
        try:
            nlist = list(map(lambda x: int(x), num_list.split(',')))
            break
        except ValueError:
            print('not all values entered are numbers')
            continue
    print (','.join([str(i ** 2) for i in nlist if i % 2 == 1]))
    return 0

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))