#!/usr/bin/env python3
import random


def main(args):
    print(random.choice(list(filter(lambda x: (x % 35 == 0), range(10, 151)))))


if __name__ == "__main__":
    import sys

    sys.exit(main(sys.argv))
