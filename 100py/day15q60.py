#!/usr/bin/env python3


def recurfunc(n):
    if n == 0:
        return 0
    else:
        return recurfunc(n - 1) + 100


def main(args):
    print(recurfunc(int(input("Enter an integer: "))))
    return 0


if __name__ == "__main__":
    import sys

    sys.exit(main(sys.argv))
