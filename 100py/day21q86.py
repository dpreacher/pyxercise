#!/usr/bin/env python3

l = [12, 24, 35, 24, 88, 120, 155]

print([i for i in l if i != 24])
