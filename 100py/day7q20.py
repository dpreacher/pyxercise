#!/usr/bin/env python3


class Generator_Divide_Seven:
    """ class to build a generator for numbers in a given range divisible by 7"""

    def __init__(self, rangelimit):
        self.range_limit = rangelimit

    def divisible_by_7(self):
        for _ in range(self.range_limit):
            if _ % 7 == 0:
                yield _


def main(args):
    max_limit = int(input("Enter a number: "))
    for i in Generator_Divide_Seven(max_limit).divisible_by_7():
        print(f"{i}")
    else:
        print(f"there are no more numbers divisible by 7 in the range 0..{max_limit}")
    return 0


if __name__ == "__main__":
    import sys

    sys.exit(main(sys.argv))
