#!/usr/bin/env python3

import string

def is_char_letter_num(c):
    if c in string.ascii_letters:
        return 's'
    elif c in string.digits:
        return 'd'

def main(args):
    in_line = input('Enter some words and numbers: ')
    letters = 0
    digits = 0
    for char in in_line:
        if is_char_letter_num(char) == 's':
            letters += 1
        elif is_char_letter_num(char) == 'd':
            digits += 1
    print (f'LETTERS {letters} \nDIGITS {digits}')
    return 0

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
