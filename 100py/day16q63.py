#!/usr/bin/env python3


def genven(num):
    for i in range(num + 1):
        if i % 2 == 0:
            yield str(i)


def main(args):
    print(",".join(genven(int(input("Enter a positive integer: ")))))


if __name__ == "__main__":
    import sys

    sys.exit(main(sys.argv))
