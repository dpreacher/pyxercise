#!/usr/bin/env python3



def main(args):
    ans_list =[]
    for _ in range(2000, 3201):
        if ((_ % 7 == 0) and (_ % 5 != 0)):
            ans_list.append(_)
    for _ in ans_list[:-1]:
        print (f'{_}', end = ',')
    print (ans_list[-1])
    return 0

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
