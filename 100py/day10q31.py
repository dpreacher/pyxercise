#!/usr/bin/env python3


def main(args):
    sqr_dict = {}
    for num in range(1, 21):
        sqr_dict[num] = num ** 2
    print(sqr_dict)
    return 0


if __name__ == "__main__":
    import sys

    sys.exit(main(sys.argv))
