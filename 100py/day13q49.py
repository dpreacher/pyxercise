#!/usr/bin/env python3


class Shape:
    def area(self):
        return "0"


class Square(Shape):
    """class to compute area of square"""

    def __init__(self, length):
        self.length = length

    def area(self):
        return self.length ** 2


def main(args):
    try:
        length = float(input("Enter the length of the square: "))
    except ValueError:
        return "Area = " + Shape().area()
    print(f"Area = {Square(length).area():.2f}")


if __name__ == "__main__":
    import sys

    sys.exit(main(sys.argv))
