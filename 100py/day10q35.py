#!/usr/bin/env python3


def main(args):
    print([num ** 2 for num in range(1, 21)][-5:])
    return 0


if __name__ == "__main__":
    import sys

    sys.exit(main(sys.argv))
