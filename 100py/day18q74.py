#!/usr/bin/env python3
import random


def main(args):
    print(random.sample(list(filter(lambda x: (x % 35 == 0), range(1, 1001))), 5))


if __name__ == "__main__":
    import sys

    sys.exit(main(sys.argv))
