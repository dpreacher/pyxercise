#!/usr/bin/env python3


def runner_up(scorelist):
    return sorted(list(set(scorelist)))[-2]


def main(args):
    count = int(input("Enter the number of scores: "))
    scores = [int(input(f"Enter score of player {i + 1}: ")) for i in range(count)]
    print(f"The runner up scored {runner_up(scores)} points")
    return 0


if __name__ == "__main__":
    import sys

    sys.exit(main(sys.argv))
