#!/usr/bin/env python3


def main(args):
    bank_log = {'D':0, 'W':0}
    print ('enter amounts deposited starting with D and amounts withdrawn starting with W')
    print ('press just enter key in a line to finish transaction log entry')
    while True:
        txn = input()
        if txn == '' or len(txn.split()) < 2:
            break
        elif txn.split()[0] == 'D':
            bank_log['D'] += int(txn.split()[1])
        elif txn.split()[0] == 'W':
            bank_log['W'] += int(txn.split()[1])
    print (bank_log['D'] - bank_log['W'])
    return 0

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))