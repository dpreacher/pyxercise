#!/usr/bin/env python3

def build_matrix(x, y):
    ans_list = []
    for i in range(x):
        in_list = []
        for j in range(y):
            in_list.append(i * j)
        ans_list.append(in_list)
    return ans_list

def main(args):
    number_list = input("Enter 2 numbers separated by commas: ")
    if (number_list.startswith(',')) or (number_list.endswith(',')) or (number_list.find('.') != -1):
        print ("write like 1,2,3,4 why act smart?")
    elif len(number_list.split(',')) == 2:
        input_sanity = True
    x, y = tuple([int(_) for _ in number_list.split(',')])
    print (f'{build_matrix(x, y)}')
    return 0

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
