#!/usr/bin/env python3


def main(args):
    stringchars = input("Enter a sequence of random characters: ")
    print(stringchars[::2])
    return 0


if __name__ == "__main__":
    import sys

    sys.exit(main(sys.argv))
