#!/usr/bin/env python3


def main(args):
    print(list(map(lambda x: x ** 2, range(1, 21))))
    return 0


if __name__ == "__main__":
    import sys

    sys.exit(main(sys.argv))
