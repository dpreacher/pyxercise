#!/usr/bin/env python3


def join_two_strs(str1, str2):
    print(f"{str1 + str2}")


def main(args):
    while True:
        try:
            str1, str2 = tuple(
                input(
                    "Enter two strings separated by comma or press enter to quit: "
                ).split(",")
            )
        except ValueError:
            break
        join_two_strs(str1, str2)
    print("ByeBye")


if __name__ == "__main__":
    import sys

    sys.exit(main(sys.argv))
