#!/usr/bin/env python3

subject = ["I", "You"]
verb = ["play", "love"]
obj = ["football", "hockey"]

for s in subject:
    for v in verb:
        for o in obj:
            print(f"{s} {v} {o}")
