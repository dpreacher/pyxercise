#!/usr/bin/env python3
import random


def main(args):
    print(random.choice([i for i in range(11) if not i % 2]))


if __name__ == "__main__":
    import sys

    sys.exit(main(sys.argv))
