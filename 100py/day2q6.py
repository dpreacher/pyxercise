#!/usr/bin/env python3

def main(args):
    input_sanity = False
    while not input_sanity:
        number_list = input("Enter a list of numbers separated by commas: ")
        if (number_list.startswith(',')) or (number_list.endswith(',')) or (number_list.find('.') != -1):
            print ("write like 1,2,3,4 why act smart?")
        else:
            input_sanity = True
    input_numbers = [float(_) for _ in number_list.split(',')]
    ans_list = list()
    for _ in input_numbers:
        ans_list.append(round(((2 * 50 * _) / 30) ** 0.5))
    print (','.join([ str(_) for _ in ans_list]))
    return 0

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
