#!/usr/bin/env python3

def main(args):
    line = input("Enter a line of words aka sentence: ")
    word_list = line.split()
    for word in sorted(set(word_list)):
        print(f'{word}:{word_list.count(word)}')
    return 0

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
