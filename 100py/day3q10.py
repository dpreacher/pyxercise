#!/usr/bin/env python3

def sort_unique_words(streng):
    ans_list = list(set(streng.split(' ')))
    ans_list.sort()
    return ans_list

def main(args):
    word_str = input("Enter a series of words with space between each of them: ")
    print (' '.join(sort_unique_words(word_str)))
    return 0

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
