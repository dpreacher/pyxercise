#!/usr/bin/env python3

from datetime import datetime

before = datetime.now()

for _ in range(10000):
    x = 1 + 1

after = datetime.now()

time_taken = after - before

print(time_taken.microseconds)
