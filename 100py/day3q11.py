#!/usr/bin/env python3

def is_num_binary(num):
    try:
        int(num, 2)
        return True
    except ValueError:
        return False

def main(args):
    while True:
        bin_list = input("Enter a comma separated sequence of binary numbers: ")
        if len([num for num in bin_list.split(',') if not is_num_binary(num)]) > 0:
            print ('Enter binary numbers with 0s and 1s only')
        else:
            break
    print(','.join([i for i in bin_list.split(',') if int(i, 2) % 5 == 0]))
    return 0

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
