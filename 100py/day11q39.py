#!/usr/bin/env python3


def main(args):
    print(tuple(i for i in range(1, 11) if i % 2 == 0))
    return 0


if __name__ == "__main__":
    import sys

    sys.exit(main(sys.argv))
