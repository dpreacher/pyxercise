#!/usr/bin/env python3


def wrap_string(s, w):2
    for i in range(0, len(s), w):
        print(s[i : i + w], end="\n")


def main(args):
    line = input("dil khol ke gaali do: ")
    per_line = int(input("how many characters per line: "))
    wrap_string(line, per_line)
    return 0


if __name__ == "__main__":
    import sys

    sys.exit(main(sys.argv))
