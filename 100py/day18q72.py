#!/usr/bin/env python3
import random


def main(args):
    print(random.sample(range(100, 201), 5))


if __name__ == "__main__":
    import sys

    sys.exit(main(sys.argv))
