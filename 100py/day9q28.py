#!/usr/bin/env python3


def add_two_num_num(num1, num2):
    print(f"The sum of {num1} and {num2} is {int(num1) + int(num2)}")


def main(args):
    while True:
        try:
            num1, num2 = tuple(
                input(
                    "Enter two numbers separated by comma or press enter to quit: "
                ).split(",")
            )
        except ValueError:
            break
        add_two_num_num(num1, num2)
    print("ByeBye")


if __name__ == "__main__":
    import sys

    sys.exit(main(sys.argv))
