#!/usr/bin/env python3


def five_by_oh():
    return 5 / 0


def main(args):
    try:
        five_by_oh()
    except ZeroDivisionError:
        print("To Infinity and Beyond!!")
    return 0


if __name__ == "__main__":
    import sys

    sys.exit(main(sys.argv))
