#!/usr/bin/env python3


def main(args):
    in_line = input('Enter some words using capital and small letters: ')
    upper, lower = (len([u for u in in_line if u.isupper()]), len([l for l in in_line if l.islower()]))
    print (f'UPPER CASE {upper} \nLOWER CASE {lower}')
    return 0

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
