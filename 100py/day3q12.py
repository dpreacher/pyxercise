#!/usr/bin/env python3

def all_digits_even(num_str):
    if (int(num_str) % 2 != 0) or (len([i for i in num_str if i not in ['0','2', '4', '6', '8']]) > 0):
        return False
    else:
        return True

def main(args):
    print(','.join([str(i) for i in range(1000,3001) if all_digits_even(str(i))]))

    return 0

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
