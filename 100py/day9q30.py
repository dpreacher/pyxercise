#!/usr/bin/env python3


def print_longest_string(s1, s2):
    if len(s1) > len(s2):
        yield s1
    elif len(s2) > len(s1):
        yield s2
    else:
        yield s1
        yield s2


def main(args):
    line1 = input("Enter a string: ")
    line2 = input("Enter another string: ")
    for line in print_longest_string(line1, line2):
        print(line)
    return 0


if __name__ == "__main__":
    import sys

    sys.exit(main(sys.argv))
