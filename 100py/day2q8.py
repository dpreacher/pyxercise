#!/usr/bin/env python3

def main(args):
    input_sanity = False
    while not input_sanity:
        word_list = input("Enter a list of words separated by commas: ")
        if (word_list.startswith(',')) or (word_list.endswith(',')) or (word_list.find('.') != -1):
            print ("write like 1,2,3,4 why acting smart?")
        else:
            input_sanity = True
        ans_list = word_list.split(",")
        ans_list.sort()
        print(f'The words you entered when sorted become {",".join(ans_list)}')

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))


