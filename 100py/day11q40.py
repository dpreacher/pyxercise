#!/usr/bin/env python3


def main(args):
    print(("Yes" if input("Enter a word: ").lower() == "yes" else "No"))
    return 0


if __name__ == "__main__":
    import sys

    sys.exit(main(sys.argv))
