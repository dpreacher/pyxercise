#!/usr/bin/env python3


def gendiv(num):
    for i in range(num + 1):
        if (i % 5 == 0) and (i % 7 == 0):
            yield str(i)


def main(args):
    print(",".join(gendiv(int(input("Enter a positive integer: ")))))


if __name__ == "__main__":
    import sys

    sys.exit(main(sys.argv))
