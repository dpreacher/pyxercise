#!/usr/bin/env python3


l = [12, 24, 35, 70, 88, 120, 155]

print([l[i] for i in range(len(l)) if i not in range(1, 4)])
