#!/usr/bin/env python3


def count_chars(s):
    count_dictula = {}
    for c in s:
        if c not in count_dictula.keys():
            count_dictula[c] = 1
        else:
            count_dictula[c] += 1
    return count_dictula


def main(args):
    stringchars = input(
        "Enter a sequence of random characters with or without repetitions: "
    )
    outdict = count_chars(stringchars)
    for i in outdict.keys():
        print(f"{i},{outdict[i]}")
    return 0


if __name__ == "__main__":
    import sys

    sys.exit(main(sys.argv))
