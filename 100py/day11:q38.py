#!/usr/bin/env python3


def main(args):
    print(tuple(range(1, 11))[:5])
    print(tuple(range(1, 11))[-5:])
    return 0


if __name__ == "__main__":
    import sys

    sys.exit(main(sys.argv))
