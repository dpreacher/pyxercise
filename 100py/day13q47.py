#!/usr/bin/env python3

from math import pi


class Circle:
    """class to compute area of circle"""

    def __init__(self, radius):
        self.radius = radius

    def circle_area(self):
        return pi * (self.radius ** 2)


def main(args):
    radius = float(input("Enter the radius of a circle: "))
    print(f'{Circle(radius).circle_area():.2f}')
    return 0


if __name__ == "__main__":
    import sys

    sys.exit(main(sys.argv))
