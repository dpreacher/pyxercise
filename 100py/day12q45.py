#!/usr/bin/env python3


class American():

    @staticmethod
    def printNationality():
        print("Murican")


def main(args):
    American.printNationality()
    return 0


if __name__ == "__main__":
    import sys

    sys.exit(main(sys.argv))
