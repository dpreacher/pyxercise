#!/usr/bin/env python3


def main(args):
    while True:
        dijit = input('Enter a single digit numeral: ')
        if (dijit.isnumeric()) and (int(dijit) in range(10)):
            break
    print (f'{int(dijit) + int(2 * dijit) + int(3 * dijit) + int(4 * dijit)}')
    return 0

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
