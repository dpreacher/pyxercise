#!/usr/bin/env python3


def main(args):
    print(
        ",".join(map(lambda i: str(i ** 2), filter(lambda x: x % 2 == 0, range(1, 11))))
    )
    return 0


if __name__ == "__main__":
    import sys

    sys.exit(main(sys.argv))
