#!/usr/bin/env python3

def main(args):
    input_sanity = False
    while not input_sanity:
        number_list = input("Enter a list of numbers separated by commas: ")
        if (number_list.startswith(',')) or (number_list.endswith(',')) or (number_list.find('.') != -1):
            print ("write like 1,2,3,4 why act smart?")
        else:
            input_sanity = True
    print (number_list.split(','))
    print (tuple(number_list.split(',')))

    return 0

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
